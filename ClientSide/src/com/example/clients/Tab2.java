package com.example.clients;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
 
public class Tab2 extends Fragment {
	WebView webicerik;
	ProgressBar gecici;
	LinearLayout linlay;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2, container, false);
        linlay=(LinearLayout)rootView.findViewById(R.id.linlay);
        gecici=(ProgressBar)rootView.findViewById(R.id.gecici);
        webicerik=(WebView)rootView.findViewById(R.id.webicerik);
        webicerik.getSettings().setJavaScriptEnabled(true);
        webicerik.setWebViewClient(new WebViewClient(){
        
            @Override
            public void onPageFinished(WebView view, String url) {
                //hide loading image
            	linlay.setVisibility(View.GONE);
                gecici.setVisibility(View.GONE);
                //show webview
                webicerik.setVisibility(View.VISIBLE);
            }

        	
        });
        webicerik.loadUrl("http://summit.webrazzi.com/");
        return rootView;
    }
 
}
