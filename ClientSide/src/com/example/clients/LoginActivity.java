package com.example.clients;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
Button loginButton;
EditText et1,et2;
public static final String MyPREFERENCES = "MyPrefs" ;
public static final String Name = "nameKey";
public static final String Email = "emailKey";
public static String name,email;
SharedPreferences sharedpreferences;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		et1=(EditText)findViewById(R.id.editText1);
		et2=(EditText)findViewById(R.id.editText2);
		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		if(sharedpreferences.getAll().isEmpty()){
			Toast.makeText(getApplication(), "Lütfen giriş yapınız", Toast.LENGTH_LONG).show();
		}else{
			name=sharedpreferences.getAll().get(Name).toString();
			email=sharedpreferences.getAll().get(Email).toString();
			Intent i=new Intent(LoginActivity.this,MainActivity.class);
			startActivity(i);
			Toast.makeText(getApplication(), "Hoşgeldiniz "+name, Toast.LENGTH_LONG).show();
		}
		
		loginButton=(Button)findViewById(R.id.loginButton);
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreferences.Editor editor = sharedpreferences.edit();
	            name=et1.getText().toString();
	            email=et2.getText().toString();
	            editor.putString(Name, name);
	            editor.putString(Email, email);
	            editor.commit();
				
				Intent i=new Intent(LoginActivity.this,MainActivity.class);
				startActivity(i);
				Toast.makeText(getApplication(), "Hoşgeldiniz "+name, Toast.LENGTH_LONG).show();
			}
		});
	}
}
