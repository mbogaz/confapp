package com.example.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.DocumentsContract.Root;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
 
public class Tab1 extends Fragment {
	public static Button startButton;
	public static TextView textview1;
	private Spinner spinner1;
	public static Context c;
	public 	String[] ip;
	public static int option=1;
	public static String html,text;
	public static boolean isPlaying;
	int playBufSize;
	static final int frequency = 8000;
	static final int channelConfiguration = AudioFormat.CHANNEL_OUT_MONO;
	static final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
	AudioTrack audioTrack;
	Socket connfd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab1, container, false);
        startButton=(Button)rootView.findViewById(R.id.start_button);
        textview1=(TextView)rootView.findViewById(R.id.textView1);
        spinner1 = (Spinner) rootView.findViewById(R.id.spinner1);
        if(option==0){
    		startButton.setBackgroundResource(R.drawable.pause_button);}
  
        new BackgroundTask().execute();
        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				html=ip[position];
				MainActivity.html=ip[position];
				MainActivity.selectedIp=position;
			}@Override
			public void onNothingSelected(AdapterView<?> parent) {}

        });
        startButton.setOnClickListener (startListener);
        html=MainActivity.html;
        return rootView;
    }
   

    private final OnClickListener startListener = new OnClickListener() {

        @Override
        public void onClick(View arg0) {
        	if(option==1){
        		startButton.setBackgroundResource(R.drawable.pause_button);
        		MainActivity.startStreaming();
                textview1.setText("SUNUCU CİHAZ ARANIYOR");
                option=0;
        	}else{
        		startButton.setBackgroundResource(R.drawable.play_button);
        		isPlaying=false;
            	textview1.setText("DURDU");
            	option=1;
        	}
        	
        }

    };
    public class BackgroundTask extends AsyncTask<Void, Void, Void> {
    	 private ProgressDialog dialog;
         
         public BackgroundTask() {
             dialog = new ProgressDialog(getActivity());
         }
    	 @Override
         protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Doing something, please wait.");
            dialog.setCancelable(false);
            dialog.show();
    	 }
    	
    	@Override
    	protected Void doInBackground(Void... params) {
    		try{ HttpClient httpclient = new DefaultHttpClient(); // Create HTTP Client
    	    HttpGet httpget = new HttpGet("http://ptvt.net//ips.txt"); // Set the action you want to do
    	    HttpResponse response = httpclient.execute(httpget); // Executeit
    	    HttpEntity entity = response.getEntity(); 
    	    InputStream is = entity.getContent(); // Create an InputStream with the response
    	    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
    	    StringBuilder sb = new StringBuilder();
    	    String line = null;
    	    while ((line = reader.readLine()) != null) // Read line by line
    	        sb.append(line + "\n");

    	    String resString = sb.toString(); // Result is here

    	    is.close(); text=resString; }catch(Exception e){}
    		return null;
    	}
    	 @Override
         protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            
            if(text==null){
            	dialog.setMessage("Şuanda aktif bir oturum bulunmamaktadır");
            }
            try{
            List<String> list = new ArrayList<String>();
            String[] datas=text.split("patavat");
            String[] layers=datas[0].split(",");
            Tab3.html2=datas[1].replace("\n", "");
            ip=new String[layers.length];
            for(int i=0;i<layers.length;i++){
            	String[] ips=layers[i].split("-");
            	list.add(ips[1].replaceAll( "UUU","Ü").replaceAll("CCC", "Ç").replaceAll("III", "İ"));
            	ip[i]=ips[0];
            }
            
        	ArrayAdapter<String> adapter_category = new ArrayAdapter<String>(getActivity().getApplicationContext(),
        		    R.layout.spinner_list_item, list);
        		adapter_category
        		    .setDropDownViewResource(R.layout.spinner_item);
        	spinner1.setAdapter(adapter_category);
        	 if (dialog.isShowing()) {
                 dialog.dismiss();
             }
            }catch(Exception e){
            	Toast.makeText(getActivity(), "error :"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
            finally{ 
            	if (dialog.isShowing()) {
                dialog.dismiss();
            }}
            spinner1.setSelection(MainActivity.selectedIp);
    	 }
    	}
}
