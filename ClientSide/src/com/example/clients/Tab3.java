package com.example.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.example.clients.Tab1.BackgroundTask;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
 
public class Tab3 extends Fragment {
	
	ImageView im1,im2,imbildirim;
	public String insertAdress,userNick,text;
	public static String html2;
	EditText editText;
	TextView textView;
	String[] questions,labels,options;
	ArrayList multiple=new ArrayList<String[]>();
	private Spinner spinner1;
	AlertDialog.Builder alertDialogBuilder;
	AlertDialog alert;
	String answerType="n";
	int soruId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab3, container, false);
        new BackgroundTask().execute();
        textView=(TextView)rootView.findViewById(R.id.textView1);
        im1=(ImageView)rootView.findViewById(R.id.imageView2);
        im2=(ImageView)rootView.findViewById(R.id.imageView3);
        imbildirim=(ImageView)rootView.findViewById(R.id.imageView4);
        imbildirim.setVisibility(View.INVISIBLE);	
        imbildirim.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(text.contains("null")){
				Toast.makeText(getActivity(), "Şuan aktif bir soru bulunmamaktadır.", Toast.LENGTH_LONG).show();
				}else
				showInputDialog(2);
			}
		});
        im1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showInputDialog(1);
			}
		});
        im2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(text.contains("null")){
				Toast.makeText(getActivity(), "Şuan aktif bir soru bulunmamaktadır.", Toast.LENGTH_LONG).show();
				}else
				showInputDialog(2);
			}
		});
        return rootView;
    }
    protected void showInputDialog(int choose) {

		// get prompts.xml view
		
if(choose==1){
	LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
	View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
	alertDialogBuilder.setView(promptView);
		userNick=LoginActivity.email+"-"+LoginActivity.name;
		editText = (EditText) promptView.findViewById(R.id.edittext);
		editText.setHint("Sorunuzu Alalım");
		// setup a dialog window
		alertDialogBuilder.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Thread thread = new Thread(new Runnable(){
						    @Override
						    public void run() {
						        try {
						        	insert(userNick,editText.getText().toString(),"sorual");
						        } catch (Exception e) {
						            e.printStackTrace();
						        }
						    }
						});

						thread.start(); 
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create an alert dialog
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();}
////////////////////////////////////////////////////////////////////////////////////////////
if(choose==2){
	LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
	final View promptView = layoutInflater.inflate(R.layout.input_spinner_dialog, null);
	final View promptView2 = layoutInflater.inflate(R.layout.input_dialog, null);
	alertDialogBuilder = new AlertDialog.Builder(getActivity());
	alertDialogBuilder.setView(promptView);
	
	userNick=LoginActivity.email+"-"+LoginActivity.name;
	editText = (EditText) promptView.findViewById(R.id.edittext);
	editText.setHint("Cevabınızı Alalım");
	spinner1=(Spinner)promptView.findViewById(R.id.spinner1);
	ArrayAdapter<String> adapter_category = new ArrayAdapter<String>(getActivity().getApplicationContext(),
		    R.layout.spinner_list_item, questions);
		adapter_category
		    .setDropDownViewResource(R.layout.spinner_item);
	spinner1.setAdapter(adapter_category);
	spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				final int position, long id) {
			soruId=position;
			if(options[position].contains("c")){
			/////////////////
			AlertDialog.Builder ab=new AlertDialog.Builder(getActivity());
			ab.setCancelable(false);
			ab.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					alert.dismiss();
				}
			});
			ab.setTitle(questions[position]);
			ab.setItems((String[])multiple.get(position), new DialogInterface.OnClickListener() {


			public void onClick(DialogInterface d, final int choice) {
				editText.setVisibility(View.INVISIBLE);
				Thread thread = new Thread(new Runnable(){
				    @Override
				    public void run() {
				        try {
				        	answerType="c";
				        	insert(userNick,(choice+1)+"","yanital");
				        } catch (Exception e) {
				            e.printStackTrace();
				        }
				    }
				});

				thread.start(); 
				
			alert.dismiss();
			Toast.makeText(getActivity(), "Cevabınız için teşekkürler", Toast.LENGTH_LONG).show();
			}
			});
			ab.show();
			//////////////////////
			}
			else{
				editText.setVisibility(View.VISIBLE);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}
	});
	// setup a dialog window
	alertDialogBuilder.setTitle("Sorular");
	alertDialogBuilder.setCancelable(true)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					Thread thread = new Thread(new Runnable(){
					    @Override
					    public void run() {
					        try {
					        	answerType="n";
					        	insert(userNick,editText.getText().toString(),"yanital");
					        } catch (Exception e) {
					            e.printStackTrace();
					        }
					    }
					});

					thread.start(); 
				}
			})
			.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});

	// create an alert dialog
	alert = alertDialogBuilder.create();
	alert.show();}
	}
    
    public boolean insert(String text1,String text2,String phpFileName)
    {
    	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
 
   	nameValuePairs.add(new BasicNameValuePair("kullanici",text1));
   	nameValuePairs.add(new BasicNameValuePair("soru",text2));
   	nameValuePairs.add(new BasicNameValuePair("tur",answerType));
   	nameValuePairs.add(new BasicNameValuePair("soruId",soruId+""));
  
    	InputStream is = null;
		try
    	{
		HttpClient httpclient = new DefaultHttpClient();
		insertAdress="http://"+html2+"/"+phpFileName+".php";
	        HttpPost httppost = new HttpPost(insertAdress);
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
	        HttpResponse response = httpclient.execute(httppost); 
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
	        Log.e("pass 1", "connection success ");
	}
        catch(Exception e)
	{
        	Log.e("Fail 1", e.toString());
	    	Toast.makeText(getActivity(), e.getMessage(),
			Toast.LENGTH_LONG).show();
	}     
        
        String result = null;
		try
        {
            BufferedReader reader = new BufferedReader
			(new InputStreamReader(is,"UTF8"),8);
            StringBuilder sb = new StringBuilder();
            String line;
			while ((line = reader.readLine()) != null)
	    {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            if(result.contains("success")){
            	return true;
            	}
	    Log.d("pass 2", "result: "+result);
	}
        catch(Exception e)
	{
            Log.e("Fail 2", e.toString());
	}     
return false;
	
    }
    public class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        
        public BackgroundTask() {
            dialog = new ProgressDialog(getActivity());
        }
   	 @Override
        protected void onPreExecute() {
           super.onPreExecute();
           dialog.setMessage("Doing something, please wait.");
           dialog.show();
   	 }
   	
   	@Override
   	protected Void doInBackground(Void... params) {
   		try{ 
		HttpClient httpclient = new DefaultHttpClient(); // Create HTTP Client
		HttpGet httpget = new HttpGet("http://"+html2+"/soru.php"); // Set the action you want to do
		HttpResponse response = httpclient.execute(httpget); // Executeit
   	    HttpEntity entity = response.getEntity(); 
   	    InputStream is = entity.getContent(); // Create an InputStream with the response
   	    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF8"), 8);
   	    StringBuilder sb = new StringBuilder();
   	    String line = null;
   	    while ((line = reader.readLine()) != null) // Read line by line
   	        sb.append(line + "\n");

   	    String resString = sb.toString(); // Result is here

   	    is.close(); 
   	    text=resString;}catch(Exception e){}
   		return null;
   	}
   	 @Override
        protected void onPostExecute(Void result) {
           super.onPostExecute(result);
           Log.e("error", text);
           if(text.contains("null")){}
           else{
            	  imbildirim.setVisibility(View.VISIBLE);	
           List<String> list = new ArrayList<String>();
           labels=text.split(",");
           questions=labels[0].split("-");
           options=labels[1].split("-");
           String[] answers=labels[2].split("-");
           int lnght=questions.length;
           try{
           for(int i=0;i<lnght;i++){
        	   String[] temp=answers[i].split(Pattern.quote("*"));
        	  multiple.add(temp);
           }}catch(Exception e){Toast.makeText(getActivity(),e.getMessage(), Toast.LENGTH_LONG).show();}
              }
           if (dialog.isShowing()) {
               dialog.dismiss();
           }
           }
   	
   	}
   

}
