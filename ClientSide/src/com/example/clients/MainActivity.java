package com.example.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.example.clients.Tab1.BackgroundTask;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity {
	ActionBar.Tab tab1,tab2,tab3;
	Fragment Tab_1 = new Tab1();
	Fragment Tab_2 = new Tab2();
	Fragment Tab_3 = new Tab3();
	static Activity a;
	public static String html,text;
	static int playBufSize;
	static final int frequency = 8000;
	static final int channelConfiguration = AudioFormat.CHANNEL_OUT_MONO;
	static final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
	static AudioTrack audioTrack;
	static Socket connfd;
	public static int selectedIp=0;
	@Override
	public void onResume() {
	    super.onResume();  // Always call the superclass method first

	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		a=(Activity) MainActivity.this;
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	
		tab2 = actionBar.newTab().setText("Tercüme");
		tab2.setTabListener(new TabListener(Tab_1));
	    actionBar.addTab(tab2);
	    tab1 = actionBar.newTab().setText("Program");
		tab1.setTabListener(new TabListener(Tab_2));
	    actionBar.addTab(tab1);
	    tab3 = actionBar.newTab().setText("Etkileşim");
		tab3.setTabListener(new TabListener(Tab_3));
	    actionBar.addTab(tab3);
	}

	public static void startStreaming() {
		html=Tab1.html;
    	playBufSize=AudioTrack.getMinBufferSize(frequency, channelConfiguration, audioEncoding);
    	audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, frequency, channelConfiguration, audioEncoding, playBufSize, AudioTrack.MODE_STREAM);
    	audioTrack.setStereoVolume(1f, 1f);
    	new Thread() {
    		byte[] buffer = new byte[1024];
    		public void run() {
    			try { connfd = new Socket(html, 3000); 
    			    a.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                         Tab1.textview1.setText("BAĞLANILDI");
                    }
                });
    			}
    			catch (Exception e) {
    				Tab1.option=1;
    				a.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        	Tab1.startButton.setBackgroundResource(R.drawable.play_button);
                            Tab1.textview1.setText("BAĞLANILAMADI");
                            Tab1.option=1;
                        }
                    });
    			}
    			audioTrack.play();
    			Tab1.isPlaying = true;
    	        while (Tab1.isPlaying) {
    	        	int readSize = 0;
    	            try { readSize = connfd.getInputStream().read(buffer); }
    	            catch (Exception e) {}
    	        	audioTrack.write(buffer, 0, readSize);
    	        }  
    	        audioTrack.stop();
    			try { connfd.close(); }
    			catch (Exception e) { e.printStackTrace(); }
    		
    		}
    		
    	}.start();
	
		 
	 }
	
}
